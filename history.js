window.onload = function () {
    
            // Get all the `button` elements
            var buttons = document.querySelectorAll("button");
            // To store the current `table` 
            var currentTable = null;
        
            // Loop over each button element and attach an handler to `click` event
            buttons.forEach((element, index, array) => {
        
            element.addEventListener("click", function () {
        
                 // Get the associated table using the `data-table` attribute of this button
                var tableId = element.getAttribute("data-table");
        
                    // Good to go? 
                    if (tableId) {
        
                        //  Hide the current `table` if we have any displayed
                        if (currentTable !== null) {
                        currentTable.style.display = "none";
                        }
        
                        // Store the current table and `display` it
                        currentTable = document.getElementById(tableId);
                        currentTable.style.display = "table";
                    }
        
                });
        
            });
        }