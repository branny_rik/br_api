/**
 * This JS File will be used for the 'latest' Currency Changes
 * We will use another JS File for the history of Currency Changes (With a dropdown menu to select YY-MM-DD)
 */

//DOM ready
    window.addEventListener('load', init);

// Init / Activate functions
    function init() {
    //    get_EUR();
    //    get_USD();
    //    get_GBP();
    //    get_CHF();
    //    get_JPY();
    //    get_CAD();
        // myFunction();
    }


// Ajax Call for EUR-Valuta
    function get_EUR() {
        var url = 'http://api.fixer.io/latest?base=EUR';
        $.getJSON(url)
            .done(success_EUR_Handler)
            .fail(failure_EUR_Handler);
    }

// Handlers --> EUR
    // Success
        function success_EUR_Handler(data) {
            console.log('EUR --> Doel: 1€ =  ', data); 
            console.log(data.rates.USD);
            // location.reload();

            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1;

            // Table maken en inserten
            var ph = document.getElementById('rates');

            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var input1 = `${prop} = ${data.rates[prop]}`;


                var tr = document.createElement('tr');

                var td_rates = document.createElement('td');
                td_rates.innerHTML = input1;
                tr.appendChild(td_rates);

                ph.appendChild(tr);
            }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
        }

    // Failure
        function failure_EUR_Handler(data) {
            console.log('Jammer --> ', data);
        }


// AJAX Call for USD-Valuta
    function get_USD() {
        var url = 'http://api.fixer.io/latest?base=USD';
        $.getJSON(url)
            .done(success_USD_Handler)
            .fail(failure_USD_Handler);
    }

// Handlers --> USD
    // Success
        function success_USD_Handler(data) {
            console.log('USD --> Doel: 1$ =   ', data);
          
            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1;




            // Table maken en inserten
            var ph = document.getElementById('rates');
                        
            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var tr = document.createElement('tr');
                        
                var td_rates = document.createElement('td');
                td_rates.innerHTML = `${prop} = ${data.rates[prop]}`;
                tr.appendChild(td_rates);
                        
                ph.appendChild(tr);
        }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
    }   

        
    // Failure
        function failure_USD_Handler(data) {
            console.log('Jammer --> ', data);
        }

// AJAX Call for GBP-Valuta
    function get_GBP() {
        var url = 'http://api.fixer.io/latest?base=GBP';
        $.getJSON(url)
            .done(success_GBP_Handler)
            .fail(failure_GBP_Handler);
    }

// Handlers --> GBP
    // Success
        function success_GBP_Handler(data) {
            console.log('GBP --> Doel: 1£ =  ', data);
                       
            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1;

            // Table maken en inserten
            var ph = document.getElementById('rates');
                        
            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var tr = document.createElement('tr');
                        
                var td_rates = document.createElement('td');
                td_rates.innerHTML = `${prop} = ${data.rates[prop]}`;
                tr.appendChild(td_rates);
                        
                ph.appendChild(tr);
        }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
    }
    // Failure
        function failure_GBP_Handler(data) {
            console.log('Jammer --> ', data);
        }

// AJAX Call for CHF-Valuta
    function get_CHF() {
        var url = 'http://api.fixer.io/latest?base=CHF';
        $.getJSON(url)
            .done(success_CHF_Handler)
            .fail(failure_CHF_Handler);
    }

// Handlers --> CHF
    // Success
        function success_CHF_Handler(data) {
            console.log('CHF --> Doel: 1Fr. =   ', data);
                
            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1; 

            // Table maken en inserten
            var ph = document.getElementById('rates');
                        
            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var tr = document.createElement('tr');
                        
                var td_rates = document.createElement('td');
                td_rates.innerHTML = `${prop} = ${data.rates[prop]}`;
                tr.appendChild(td_rates);
                        
                ph.appendChild(tr);
        }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
    }
    // Failure
        function failure_CHF_Handler(data) {
            console.log('Jammer --> ', data);
        }

// AJAX Call for JPY-Valuta
    function get_JPY() {
        var url = 'http://api.fixer.io/latest?base=JPY';
        $.getJSON(url)
            .done(success_JPY_Handler)
            .fail(failure_JPY_Handler);
    }

// Handlers --> JPY
    // Success
        function success_JPY_Handler(data) {
            console.log('JPY --> Doel: 1¥ =   ', data);

            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1;
            
            // Table maken en inserten
            var ph = document.getElementById('rates');
                        
            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var tr = document.createElement('tr');
                        
                var td_rates = document.createElement('td');
                td_rates.innerHTML = `${prop} = ${data.rates[prop]}`;
                tr.appendChild(td_rates);
                        
                ph.appendChild(tr);
        }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
    }
    // Failure
        function failure_JPY_Handler(data) {
            console.log('Jammer --> ', data);
        }

// AJAX Call for CAD-Valuta
    function get_CAD() {
        var url = 'http://api.fixer.io/latest?base=CAD';
        $.getJSON(url)
            .done(success_CAD_Handler)
            .fail(failure_CAD_Handler);
    }

// Handlers --> CAD
    // Success
        function success_CAD_Handler(data) {
            console.log('CAD --> Doel: 1$ =  ', data);

            // Date
            var ph_date = document.getElementById('date');
            var input1 = `${data.date}`;
            ph_date.innerHTML = 'Date: ' + input1;
            
            // Base Currency
            var ph_base = document.getElementById('base');
            var input1 = `${data.base}`;
            ph_base.innerHTML = 'Base Currency: ' + input1;
            
            // Table maken en inserten
            var ph = document.getElementById('rates');
                        
            for (var prop in data.rates) {
                console.log(`${prop} = ${data.rates[prop]}`);
                var tr = document.createElement('tr');
                        
                var td_rates = document.createElement('td');
                td_rates.innerHTML = `${prop} = ${data.rates[prop]}`;
                tr.appendChild(td_rates);
                        
                ph.appendChild(tr);
        }
                var linebreak = document.createElement("br");
                ph.appendChild(linebreak)
    }
    // Failure
        function failure_CAD_Handler(data) {
            console.log('Jammer --> ', data);
        }

// Gewoon wat tekst als placeholder -- 
// Dropdown Menu ---> Hoofd Valuta
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }